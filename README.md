**Bienvenue sur le dépôt de code du projet RestEAUr'Lag**

Vous trouverez ici le [script d'installation](scripts/sh/geoserver_install.sh) du serveur mis en place dans le cadre du projet, ainsi que d'autre fichiers visant à faciliter le traitement des données avec python ou le sql.

Dans le dossier [docs](docs) se trouve un [un texte explicatif](docs/IDG.md) pour comprendre l'infrastructure de données mise en place.

Enfin, dans le dossier [html](html) se trouve un exemple de site cartographique réalisé avec Leaflet en utilisant les géoservices mis en place pour le projet.