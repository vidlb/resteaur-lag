const map = L.map('map').setView([43.54202, 3.97487], 12)

const OSM_France = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
							   {maxZoom: 20, attribution: 'Openstreetmap France | OpenStreetMap contributors'})
const ESRI_Imagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
								 {attribution: 'ESRI'})

const geoserverLink = "http://resteaur-lag.teledetection.fr/geoserver/wms?"
const qgisserverLink  = "http://resteaur-lag.teledetection.fr/qgisserver?&SERVICE=WMS&REQUEST=GetMap"

const s2_20190627_rgb = L.tileLayer.wms(geoserverLink, {layers: 's2_20190627', styles: 's2_20190627_rgb', transparent: 'true', format: 'image/png'})
const s2_20190627_nir = L.tileLayer.wms(geoserverLink, {layers: 's2_20190627', styles: 's2_20190627_nir', transparent: 'true', format: 'image/png'})
const bdoh = L.tileLayer.wms(geoserverLink, {layers: 'bdoh_herault', transparent: 'true', format: 'image/png'})
const pvah = L.tileLayer.wms(geoserverLink, {layers: 'ifremer_pvah_herault', transparent: 'true', format: 'image/png'})
const litto3d = L.tileLayer.wms(geoserverLink, {layers: 'mnt_1m_herault', transparent: 'true', format: 'image/png'})
const etat_major = L.tileLayer.wms(geoserverLink,  {layers: 'em_herault', transparent: 'true', format: 'image/png'})
const cassini = L.tileLayer.wms(geoserverLink,  {layers: 'cassini_loc_herault', transparent: 'true', format: 'image/png'})

const ombrage_1m = L.tileLayer.wms(geoserverLink, {layers: 'ombrage_1m_herault', transparent: 'true', format: 'image/png'})
const ombrage_5m = L.tileLayer.wms(geoserverLink, {layers: 'ombrage_5m_herault', transparent: 'true', format: 'image/png'})
const ramsar = L.tileLayer.wms(geoserverLink, {layers: 'sites_ramsar', transparent: 'true', format: 'image/png'})
const em_mauguio = L.tileLayer.wms(qgisserverLink + "&MAP=/data/qgisserver/em_40k_mauguio/em_40k_mauguio.qgs",
								   {layers: 'mauguio_l,mauguio_s', opacity: 0.5, transparent: 'true', format: 'image/png'})

var baseLayers = {'OSM France': OSM_France,
		  'ESRI Imagery': ESRI_Imagery,
		  'Sentinel2 - Juin 2019 (RGB)': s2_20190627_rgb,
		  'Sentinel2 - Juin 2019 (NIR)': s2_20190627_nir,
		  'BDOrtho historique': bdoh,
	      'PVA 1937': pvah,
		  'Carte d\'état major': etat_major,
		  'Cassini (LOC)': cassini,
	  	  'Litto 3D': litto3d}

var overlays = {'Ombrage Litto 3D 1m': ombrage_1m,
				'Ombrage RGEAlti 5m': ombrage_5m,
				'Sites RAMSAR': ramsar,
				'Etat major Mauguio': em_mauguio}

s2_20190627_rgb.addTo(map)
L.control.layers(baseLayers, overlays).addTo(map)
L.control.scale().addTo(map)
